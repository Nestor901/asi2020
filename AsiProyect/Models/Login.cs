﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsiProyect.Models
{
    public class Login
    {
        public int id { get; set; }
        public string user { get; set; }
        public string password { get; set; }
    }
}
