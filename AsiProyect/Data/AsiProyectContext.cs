﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using AsiProyect.Models;

namespace AsiProyect.Data
{
    public class AsiProyectContext : DbContext
    {
        public AsiProyectContext (DbContextOptions<AsiProyectContext> options)
            : base(options)
        {
        }

        public DbSet<AsiProyect.Models.prueba> prueba { get; set; }

        public DbSet<AsiProyect.Models.Login> Login { get; }
    }
}
